.DELETE_ON_ERROR:

launch: launch.c
	gcc launch.c -o launch -O2 -Wall -lcap
	setcap cap_setuid,cap_setgid,cap_dac_override,cap_sys_admin,cap_sys_ptrace,cap_setpcap+p launch
