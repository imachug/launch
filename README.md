# launch

`launch` is a lightweight sandbox for Linux, built with Linux namespaces.

`launch` must have admin capabilities for anything serious. The following caps are used:

- `CAP_SETUID`, `CAP_SETGID`, `CAP_DAC_OVERRIDE` -- to have several users and groups in sandbox
- `CAP_SYS_PTRACE` -- for inter-process communication during bootstrap and shutdown
- `CAP_SYS_ADMIN` -- to create network tunnels between several processes
- `CAP_SETPCAP` -- to set ambient capabilities for scripts


## Build

```
$ sudo make
```

This compiles the utility at `./launch` and enables all required capabilities on the executable.


## Usage

To create a user namespace, like `unshare`:

```
$ launch -U -u0 -g0 bash
# id
uid=0(root) gid=0(root) groups=0(root),65534(nogroup)
```
