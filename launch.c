// Required capabilities:
// CAP_SETUID, CAP_SETGID, CAP_DAC_OVERRIDE, CAP_SYS_PTRACE, CAP_SYS_ADMIN, CAP_SETPCAP


#define _GNU_SOURCE
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <pwd.h>
#include <sched.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/capability.h>
#include <sys/mount.h>
#include <sys/prctl.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <unistd.h>


#define STACK_SIZE 65536


void try_unshare(int flags) {
	if(unshare(flags) == -1) {
		perror("unshare");
		exit(1);
	}
}


cap_t cap_proc;


#define CONFIGURE_CAPABILITY(value, ...) do { \
	cap_value_t caps[] = {__VA_ARGS__}; \
	if(cap_set_flag(cap_proc, CAP_EFFECTIVE, sizeof(caps) / sizeof(caps[0]), caps, value) == -1) { \
		perror("cap_set_flag"); \
		exit(2); \
	} \
	if(cap_set_proc(cap_proc) == -1) { \
		perror("cap_set_proc"); \
		exit(1); \
	} \
} while(0)

#define RAISE_CAPABILITY(...) CONFIGURE_CAPABILITY(CAP_SET, __VA_ARGS__);
#define DROP_CAPABILITY(...) CONFIGURE_CAPABILITY(CAP_CLEAR, __VA_ARGS__);


typedef struct {
	id_t slave;
	id_t master;
	int established;
} IDMapping;

int id_mapping_cmp(const void* aptr, const void* bptr) {
	const IDMapping* a = aptr;
	const IDMapping* b = bptr;
	if(a->master < b->master) {
		return -1;
	} else if(a->master > b->master) {
		return 1;
	} else {
		return 0;
	}
}

IDMapping* binsearch_mapping(id_t id, size_t cnt, IDMapping* mappings) {
	size_t l = 0;
	size_t r = cnt;
	while(r - l > 1) {
		size_t m = l + (r - l) / 2;
		if(mappings[m].master > id) {
			r = m;
		} else {
			l = m;
		}
	}

	if(l < cnt && mappings[l].master == id) {
		return &mappings[l];
	} else {
		return NULL;
	}
}


int unshare_flags = 0;
IDMapping uid_mappings[64];
size_t cnt_uid_mappings = 0;
IDMapping gid_mappings[64];
size_t cnt_gid_mappings = 0;
int allow_setgroups = 0;


void write_mapping(size_t cnt_mappings, IDMapping* mappings, const char* map_name) {
	char map_path[128];
	if(snprintf(map_path, sizeof(map_path), "/proc/%jd/%s", (intmax_t)getppid(), map_name) >= sizeof(map_path)) {
		perror("snprintf");
		exit(2);
	}

	RAISE_CAPABILITY(CAP_SETUID, CAP_SETGID, CAP_DAC_OVERRIDE);

	int file;
	if((file = open(map_path, O_WRONLY)) == -1) {
		perror("open");
		exit(2);
	}

	char buf[4096];
	char* bufptr = buf;
	for(size_t i = 0; i < cnt_mappings; i++) {
		size_t left = buf + sizeof(buf) - bufptr;
		size_t written = snprintf(bufptr, left, "%u %u 1\n", mappings[i].slave, mappings[i].master);
		if(written >= left) {
			fprintf(stderr, "Too many -u options.");
			exit(2);
		}
		bufptr += written;
	}

	ssize_t written;
	if((written = write(file, buf, bufptr - buf)) != bufptr - buf) {
		if(written == -1) {
			perror("write");
			exit(2);
		}
		fprintf(stderr, "Could not write gid_map\n");
		exit(1);
	}

	if(close(file) == -1) {
		perror("close");
		exit(2);
	}

	DROP_CAPABILITY(CAP_SETUID, CAP_SETGID, CAP_DAC_OVERRIDE);
}


int helper_proc(void* pipefd_ptr) {
	int* pipefd = pipefd_ptr;


	// It may make sense to update process capabilities after fork
	cap_proc = cap_get_proc();
	if(cap_proc == NULL) {
		perror("cap_get_proc");
		return 2;
	}


	// Wait for parent to close writing side
	if(close(pipefd[1]) == -1) {
		perror("close");
		return 2;
	}
	char buf[1];
	while(read(pipefd[0], buf, 1) == -1) {
		printf("reading from helper...");
		if(errno != EAGAIN) {
			perror("read");
			return 2;
		}
	}


	if(cnt_uid_mappings > 0) {
		write_mapping(cnt_uid_mappings, uid_mappings, "uid_map");
	}

	if((unshare_flags & CLONE_NEWUSER) && !allow_setgroups) {
		char setgroups_path[128];
		if(snprintf(setgroups_path, sizeof(setgroups_path), "/proc/%jd/setgroups", (intmax_t)getppid()) >= sizeof(setgroups_path)) {
			perror("snprintf");
			exit(2);
		}

		RAISE_CAPABILITY(CAP_DAC_OVERRIDE);

		int file;
		if((file = open(setgroups_path, O_WRONLY)) == -1) {
			perror("open");
			exit(2);
		}

		ssize_t written;
		if((written = write(file, "deny", 4)) != 4) {
			if(written == -1) {
				perror("write");
				exit(2);
			}
			fprintf(stderr, "Could not write setgroups.\n");
			exit(1);
		}

		if(close(file) == -1) {
			perror("close");
			exit(2);
		}

		DROP_CAPABILITY(CAP_DAC_OVERRIDE);
	}

	if(cnt_gid_mappings > 0) {
		write_mapping(cnt_gid_mappings, gid_mappings, "gid_map");
	}

	return 0;
}


int can_masquerade_pwent(struct passwd* ent, int failure_is_fatal) {
	if(getuid() == 0 || getuid() == ent->pw_uid) {
		return 1;
	}


	RAISE_CAPABILITY(CAP_SETUID, CAP_SETGID);
	if(seteuid(ent->pw_uid) == -1) {
		perror("seteuid");
		exit(2);
	}
	if(setegid(ent->pw_gid) == -1) {
		perror("setegid");
		exit(2);
	}

	int trusts = 0;

	struct stat statbuf;
	if(stat(ent->pw_dir, &statbuf) == -1) {
		if(!failure_is_fatal) {
			goto rollback;
		}
		fprintf(stderr, "Cannot stat %s (%s home directory).\n", ent->pw_dir, ent->pw_name);
		exit(1);
	} else if(statbuf.st_uid != ent->pw_uid) {
		if(!failure_is_fatal) {
			goto rollback;
		}
		fprintf(stderr, "%s is not owned by %s.\n", ent->pw_dir, ent->pw_name);
		exit(1);
	} else if(statbuf.st_mode & S_IWOTH) {
		if(!failure_is_fatal) {
			goto rollback;
		}
		fprintf(stderr, "%s (%s home directory) is world-writable.\n", ent->pw_dir, ent->pw_name);
		exit(1);
	}

	size_t length = strlen(ent->pw_dir) + 13;
	char* masquerade_path = malloc(length);
	if(snprintf(masquerade_path, length, "%s/.masquerade", ent->pw_dir) >= length) {
		perror("snprintf");
		exit(2);
	}

	if(stat(masquerade_path, &statbuf) == -1) {
		free(masquerade_path);
		if(!failure_is_fatal) {
			goto rollback;
		}
		fprintf(stderr, "Cannot stat %s.\n", masquerade_path);
		exit(1);
	} else if(statbuf.st_uid != ent->pw_uid) {
		free(masquerade_path);
		if(!failure_is_fatal) {
			goto rollback;
		}
		fprintf(stderr, "%s is not owned by %s.\n", masquerade_path, ent->pw_name);
		exit(1);
	} else if(statbuf.st_mode & S_IWOTH) {
		free(masquerade_path);
		if(!failure_is_fatal) {
			goto rollback;
		}
		fprintf(stderr, "%s is world-writable.\n", masquerade_path);
		exit(1);
	}

	FILE* f = fopen(masquerade_path, "r");
	if(f == NULL) {
		free(masquerade_path);
		if(!failure_is_fatal) {
			goto rollback;
		}
		perror("fopen");
		exit(1);
	}

	int res;
	uid_t trusted_user;
	while((res = fscanf(f, "%u", &trusted_user)) == 1) {
		if(trusted_user == getuid()) {
			trusts = 1;
			break;
		}
	}

	if(fclose(f) == EOF) {
		perror("fclose");
		exit(2);
	}

	if(trusts) {
		free(masquerade_path);
	} else {
		if(!failure_is_fatal) {
			free(masquerade_path);
			goto rollback;
		}
		fprintf(stderr, "%u is missing from %s.\n", getuid(), masquerade_path);
		free(masquerade_path);
		exit(1);
	}

rollback:
	if(seteuid(getuid()) == -1) {
		perror("seteuid");
		exit(2);
	}
	if(setegid(getgid()) == -1) {
		perror("setegid");
		exit(2);
	}
	DROP_CAPABILITY(CAP_SETUID, CAP_SETGID);

	return trusts;
}


int can_masquerade_uid(uid_t uid, int failure_is_fatal) {
	errno = 0;
	struct passwd* ent = getpwuid(uid);
	if(ent == NULL) {
		if(errno) {
			perror("getpwuid");
			exit(2);
		}
		fprintf(stderr, "UID %u does not exist.\n", uid);
		exit(2);
	}
	return can_masquerade_pwent(ent, failure_is_fatal);
}


int openproc(pid_t pid) {
	char proc_path[128];
	if(snprintf(proc_path, sizeof(proc_path), "/proc/%jd", (intmax_t)pid) >= sizeof(proc_path)) {
		perror("snprintf");
		exit(2);
	}
	int fd;
	if((fd = open(proc_path, O_DIRECTORY)) == -1) {
		perror("open");
		exit(1);
	}
	return fd;
}


uid_t get_proc_owner_uid(int procfd) {
	int statusfd;
	if((statusfd = openat(procfd, "status", 0)) == -1) {
		perror("openat");
		exit(1);
	}

	FILE* fstatus;
	if((fstatus = fdopen(statusfd, "r")) == NULL) {
		perror("fdopen");
		exit(1);
	}

	char* str = NULL;
	size_t len = 0;
	while(getline(&str, &len, fstatus) != -1) {
		if(strncmp(str, "Uid:", 4) == 0) {
			uid_t uid;

			if(sscanf(str, "%*s%u%*u%*u%*u", &uid) < 1) {
				fprintf(stderr, "Cannot parse /proc/*/status UID field.\n");
				exit(2);
			}
			if(fclose(fstatus) == -1) {
				perror("fclose");
				exit(1);
			}
			return uid;
		}
	}
	if(ferror(fstatus)) {
		perror("getline");
		exit(2);
	}
	fprintf(stderr, "No UID field in /proc/*/status.\n");
	exit(2);
}


void resolve_proc_exe(int procfd, dev_t* dev, ino_t* inode) {
	RAISE_CAPABILITY(CAP_SYS_PTRACE);

	struct stat statbuf;
	if(fstatat(procfd, "exe", &statbuf, 0) == -1) {
		perror("fstatat");
		exit(2);
	}
	*dev = statbuf.st_dev;
	*inode = statbuf.st_ino;

	DROP_CAPABILITY(CAP_SYS_PTRACE);
}


void _resolve_file_dev_inode(const char* path, dev_t* dev, ino_t* inode) {
	struct stat statbuf;
	if(stat(path, &statbuf) == -1) {
		perror("stat");
		exit(2);
	}
	*dev = statbuf.st_dev;
	*inode = statbuf.st_ino;
}

#define RESOLVE_SELF_NS(name, dev, inode) _resolve_file_dev_inode("/proc/self/ns/" name, dev, inode)


typedef struct {
	const char* pathname;
	char* const* argv;
	int use_path;
} execv_ret_info;

int execv_ret_helper(void* info_ptr) {
	execv_ret_info* info = info_ptr;
	if(info->use_path) {
		execvp(info->pathname, info->argv);
		perror("execvp");
	} else {
		execv(info->pathname, info->argv);
		perror("execv");
	}
	return 127;
}

int exec_ret(int use_path, const char* pathname, char* const* argv) {
	char* stack = malloc(STACK_SIZE);
	if(stack == NULL) {
		fprintf(stderr, "No memory for stack.\n");
		return 127;
	}

	execv_ret_info* info = malloc(sizeof(execv_ret_info));
	if(info == NULL) {
		free(stack);
		fprintf(stderr, "No memory for execv_ret_info.\n");
		return 127;
	}

	info->pathname = pathname;
	info->argv = argv;
	info->use_path = use_path;

	int ret = 127;

	pid_t child;
	if((child = clone(execv_ret_helper, stack + STACK_SIZE, SIGCHLD, info)) == -1) {
		perror("clone");
		goto fail;
	}

	int retcode;
	if(waitpid(child, &retcode, 0) == -1) {
		perror("waitpid");
		goto fail;
	}
	if(!WIFEXITED(retcode)) {
		fprintf(stderr, "Helper process crashed.\n");
		goto fail;
	}

	ret = WEXITSTATUS(retcode);

fail:
	free(info);
	free(stack);
	return ret;
}

int execv_ret(const char* pathname, char* const* argv) {
	return exec_ret(0, pathname, argv);
}
int execvp_ret(const char* pathname, char* const* argv) {
	return exec_ret(1, pathname, argv);
}

int execl_ret(const char* pathname, ...) {
	va_list args;
	va_start(args, pathname);

	va_list tmpargs;
	va_copy(tmpargs, args);
	size_t cntargs = 0;
	do {
		cntargs++;
	} while(va_arg(tmpargs, char*));
	va_end(tmpargs);

	char** argv = malloc(cntargs * sizeof(char*));
	if(argv == NULL) {
		fprintf(stderr, "No memory for argv.\n");
		return 127;
	}
	for(size_t i = 0; i < cntargs; i++) {
		argv[i] = va_arg(args, char*);
	}
	va_end(args);

	int ret = execv_ret(pathname, argv);
	free(argv);
	return ret;
}


void get_random_string(size_t length, char* ptr) {
	length--; // null byte
	ptr[length] = 0;

	FILE* f = fopen("/dev/urandom", "rb");
	if(f == NULL) {
		perror("fopen");
		exit(2);
	}

	for(size_t i = 0; i < length; i++) {
		int ch;
		if((ch = fgetc(f)) == EOF) {
			perror("fgetc");
			exit(2);
		}
		ptr[i] = 'a' + ch % 26;
	}

	if(fclose(f) == EOF) {
		perror("fclose");
		exit(2);
	}
}


void read_autocont(int fd, void* buf, size_t count) {
	fprintf(stderr, "Reading from fd %d\n", fd);
	size_t cntread;
	while(count > 0 && (cntread = read(fd, buf, count)) != -1 && cntread != 0) {
		count -= cntread;
		buf += cntread;
	}
	if(count > 0) {
		if(cntread == 0) {
			fprintf(stderr, "Slave process crashed.\n");
			exit(2);
		} else {
			perror("read");
			exit(2);
		}
	}
}

void write_autocont(int fd, const void* buf, size_t count) {
	size_t written;
	while(count > 0 && (written = write(fd, buf, count)) != -1) {
		count -= written;
		buf += written;
	}
	if(count > 0) {
		perror("write3");
		fprintf(stderr, "%d %d\n", getpid(), fd);
		system("ls -la /proc/self/fd");
		for(;;) {
			sleep(1);
		}
		exit(2);
	}
}


void copy_child_exitcode(pid_t child) {
	// Watch and wait for child
	int retcode;
	if(waitpid(child, &retcode, 0) == -1) {
		perror("waitpid");
		exit(2);
	}
	if(!WIFEXITED(retcode)) {
		if(WIFSIGNALED(retcode)) {
			raise(WTERMSIG(retcode));
		}
		fprintf(stderr, "Child crashed.\n");
		exit(2);
	}
	exit(WEXITSTATUS(retcode));
}


int main(int argc, char** argv, char** envp) {
	if(argc <= 1) {
		fprintf(stderr, "Usage: %s\n", argv[0]);
		fprintf(stderr, "         [-inmpUhD] [-u slave=master]+ [-g slave=master]+\n");
		fprintf(stderr, "         [-O options|-R path] [-M[spPSQrt]+] [-H hostname] [-L uid:gid]\n");
		fprintf(stderr, "         [-E command] [-A command] [-P fd] [-C capability]+ [--]\n");
		fprintf(stderr, "         program args...\n");
		fprintf(stderr, "       -or-\n");
		fprintf(stderr, "         -l type apid:aname bpid:bname [more args...]\n");
		fprintf(stderr, "       -or-\n");
		fprintf(stderr, "         -j fd [-j fd]... program args...\n");
		fprintf(stderr, "Highly customizable sandbox for Linux.\n");
		fprintf(stderr, "\n");
		fprintf(stderr, "-i                  Create a new IPC namespace\n");
		fprintf(stderr, "-n                  Create a new network namespace\n");
		fprintf(stderr, "-m                  Create a new mount namespace\n");
		fprintf(stderr, "-p                  Create a new PID namespace and fork\n");
		fprintf(stderr, "-U                  Create a new user namespace. You must map root user\n");
		fprintf(stderr, "                    with -u 0=... or -u 0\n");
		fprintf(stderr, "-h                  Create a new UTS namespace\n");
		fprintf(stderr, "-D                  Allow sandboxed process to execute setgroups().\n");
		fprintf(stderr, "                    Requires -U. Caller must be root. See CVE-2014-8989\n");
		fprintf(stderr, "-u slave[=master]   Map 'master' UID from outside sandbox to 'slave' UID\n");
		fprintf(stderr, "                    inside sandbox. Requires -U. The 'master' UID must\n");
		fprintf(stderr, "                    match caller process UID, or the caller must be\n");
		fprintf(stderr, "                    root, or the 'master' user home directory must\n");
		fprintf(stderr, "                    contain .masquerade file with caller UID on one of\n");
		fprintf(stderr, "                    its lines, and both the home and .masquerade file\n");
		fprintf(stderr, "                    must be owned by 'master' and not be world-writable.\n");
		fprintf(stderr, "                    Multiple -u options may be passed. If '=master' is\n");
		fprintf(stderr, "                    not specified, caller is assumed\n");
		fprintf(stderr, "-g slave[=master]   Map 'master' GID from outside sandbox to 'slave' GID\n");
		fprintf(stderr, "                    inside sandbox. Requires -U. The 'master' GID must\n");
		fprintf(stderr, "                    match caller process GID, or the caller must be\n");
		fprintf(stderr, "                    root, or some user whose primary group is 'master'\n");
		fprintf(stderr, "                    must have a home directory which contains\n");
		fprintf(stderr, "                    .masquerade file with caller UID on one of its\n");
		fprintf(stderr, "                    lines, and both the home and .masquerade file must\n");
		fprintf(stderr, "                    be group-owned by 'master' and not be\n");
		fprintf(stderr, "                    world-writable. Multiple -g options may be passed.\n");
		fprintf(stderr, "                    If '=master' is not specified, caller is assumed\n\n");
		fprintf(stderr, "-O options          Mount overlayfs with 'options' as root directory.\n");
		fprintf(stderr, "                    Requires -m and root in child namespace\n");
		fprintf(stderr, "-R path             Bind-mount 'path' as root directory. Requires -m and\n");
		fprintf(stderr, "                    root in child namespace\n");
		fprintf(stderr, "-M filesystems      Mount filesystems inside new root. Requires -O/-R.\n");
		fprintf(stderr, "                    s: /sys\n");
		fprintf(stderr, "                    p: /proc\n");
		fprintf(stderr, "                    P: /dev/pts\n");
		fprintf(stderr, "                    S: /dev/shm\n");
		fprintf(stderr, "                    Q: /dev/mqueue\n");
		fprintf(stderr, "                    r: /run\n");
		fprintf(stderr, "                    t: /tmp\n");
		fprintf(stderr, "-H hostname         Change hostname to 'hostname'. Requires -h\n");
		fprintf(stderr, "-L uid:gid          Login as 'uid' user, 'gid' group. Requires -U.\n");
		fprintf(stderr, "                    Defaults to -L 0:0\n");
		fprintf(stderr, "-E command          Execute a shell script before descending into\n");
		fprintf(stderr, "                    sandboxed root and PID namespace. The command is run\n");
		fprintf(stderr, "                    under root if -U is passed and caller UID/GID\n");
		fprintf(stderr, "                    otherwise\n");
		fprintf(stderr, "-A command          Execute a shell script before the main command. The\n");
		fprintf(stderr, "                    command is run under root if -U is passed and caller\n");
		fprintf(stderr, "                    UID/GID otherwise\n");
		fprintf(stderr, "-P fd               Use file descriptor 'fd' for synchronization. Stop\n");
		fprintf(stderr, "                    and wait for a signal from -j. This is performed\n");
		fprintf(stderr, "                    after -E but before -A\n");
		fprintf(stderr, "-C capability       Add a capability to ambient set. A good alternative\n");
		fprintf(stderr, "                    to -L 0:0. Requires -U\n");
		fprintf(stderr, "\n");
		fprintf(stderr, "-l type apid:aname  Create network tunnel between network namespaces of\n");
		fprintf(stderr, "   bpid:bname       processes 'apid' and 'bpid' with interface names\n");
		fprintf(stderr, "   [more args...]   'aname' and 'bname' respectively. The following\n");
		fprintf(stderr, "                    types are supported:\n");
		fprintf(stderr, "                    1. 'veth': The first argument is peer, the second\n");
		fprintf(stderr, "                    argument is interface. In practice, the order does\n");
		fprintf(stderr, "                    not matter. There are no other arguments.\n");
		fprintf(stderr, "                    2. 'macvlan' / 'macvtap' / 'ipvlan' / 'ipvtap': The\n");
		fprintf(stderr, "                    first argument is sub-interface, the second one is\n");
		fprintf(stderr, "                    link. The third argument is mode.\n");
		fprintf(stderr, "                    'apid' and 'bpid' must be 'launch' processes, owned\n");
		fprintf(stderr, "                    by the same user as 'launch -l' or someone who has\n");
		fprintf(stderr, "                    caller UID in the .masquerade file. This restriction\n");
		fprintf(stderr, "                    does not apply to root. Requires -n in both\n");
		fprintf(stderr, "                    processes. Notice that if 'launch' with PID 1\n");
		fprintf(stderr, "                    started two 'launch' processes with PIDs 2 and 3,\n");
		fprintf(stderr, "                    PID 2 can use 'launch -l' to access PID 3 and even\n");
		fprintf(stderr, "                    PID 1. Use -p for child 'launch'es if this is a\n");
		fprintf(stderr, "                    problem\n");
		fprintf(stderr, "\n");
		fprintf(stderr, "-j fd [-j fd...]    Use file descriptors 'fd...' for synchronization.\n");
		fprintf(stderr, "   program args...  See -P. Wait till all 'launch' processes stop, run\n");
		fprintf(stderr, "                    code and then continue them\n");
		return 0;
	}


	cap_proc = cap_get_proc();
	if(cap_proc == NULL) {
		perror("cap_get_proc");
		return 2;
	}


	if(argc >= 2 && strcmp(argv[1], "-l") == 0) {
		if(argc == 2) {
			fprintf(stderr, "Link type is missing.\n");
			return 1;
		}

		enum {
			TYPE_VETH,
			TYPE_MACVLAN,
			TYPE_MACVTAP,
			TYPE_IPVLAN,
			TYPE_IPVTAP
		} type;
		int cntargs;
		if(strcmp(argv[2], "veth") == 0) {
			type = TYPE_VETH;
			cntargs = 2;
		} else if(strcmp(argv[2], "macvlan") == 0) {
			type = TYPE_MACVLAN;
			cntargs = 3;
		} else if(strcmp(argv[2], "macvtap") == 0) {
			type = TYPE_MACVTAP;
			cntargs = 3;
		} else if(strcmp(argv[2], "ipvlan") == 0) {
			type = TYPE_IPVLAN;
			cntargs = 3;
		} else if(strcmp(argv[2], "ipvtap") == 0) {
			type = TYPE_IPVTAP;
			cntargs = 3;
		} else {
			fprintf(stderr, "Unknown link type: %s.\n", argv[2]);
			return 1;
		}

		assert(cntargs >= 2);
		if(argc != 3 + cntargs) {
			fprintf(stderr, "-l %s option takes exactly %d arguments.\n", argv[2], cntargs);
			return 1;
		}

		pid_t apid;
		char aname[64];
		if(sscanf(argv[3], "%u:%63s", &apid, aname) < 2) {
			fprintf(stderr, "-Aveth option first argument uses 'apid:aname' format.\n");
			return 1;
		}

		pid_t bpid;
		char bname[64];
		if(sscanf(argv[4], "%u:%63s", &bpid, bname) < 2) {
			fprintf(stderr, "-Aveth option second argument uses 'bpid:bname' format.\n");
			return 1;
		}


		struct stat self_stat;
		if(stat("/proc/self/exe", &self_stat) == -1) {
			perror("stat");
			return 2;
		}
		dev_t self_dev = self_stat.st_dev;
		ino_t self_inode = self_stat.st_ino;


		dev_t dev;
		ino_t inode;

		int afd = openproc(apid);
		if(!can_masquerade_uid(get_proc_owner_uid(afd), 1)) {
			fprintf(stderr, "The owner of process %u does not trust the caller.\n", apid);
			return 1;
		}
		resolve_proc_exe(afd, &dev, &inode);
		if(dev != self_dev || inode != self_inode) {
			fprintf(stderr, "Process %u is not a 'launch' process.\n", apid);
			return 1;
		}

		int bfd = openproc(bpid);
		if(!can_masquerade_uid(get_proc_owner_uid(bfd), 1)) {
			fprintf(stderr, "The owner of process %u does not trust the caller.\n", bpid);
			return 1;
		}
		resolve_proc_exe(bfd, &dev, &inode);
		if(dev != self_dev || inode != self_inode) {
			fprintf(stderr, "Process %u is not a 'launch' process.\n", bpid);
			return 1;
		}


		RAISE_CAPABILITY(CAP_SYS_PTRACE);
		int anetfd;
		if((anetfd = openat(afd, "ns/net", 0)) == -1) {
			perror("openat");
			return 1;
		}
		DROP_CAPABILITY(CAP_SYS_PTRACE);

		dev_t orig_net_dev;
		ino_t orig_net_inode;
		RESOLVE_SELF_NS("net", &orig_net_dev, &orig_net_inode);

		RAISE_CAPABILITY(CAP_SYS_ADMIN);
		if(setns(anetfd, CLONE_NEWNET) == -1) {
			perror("setns");
			return 1;
		}
		DROP_CAPABILITY(CAP_SYS_ADMIN);

		RESOLVE_SELF_NS("net", &dev, &inode);

		if(dev == orig_net_dev && inode == orig_net_inode) {
			fprintf(stderr, "Process %u was not started with -n option, or has not unshared network namespace yet.\n", apid);
			return 1;
		}


		char tmpname[11];
		get_random_string(sizeof(tmpname), tmpname);

		int child = fork();
		if(child == -1) {
			perror("fork");
			return 2;
		}

		if(child == 0) {
			cap_proc = cap_get_proc();
			if(cap_proc == NULL) {
				perror("cap_get_proc");
				return 2;
			}


			RAISE_CAPABILITY(CAP_SYS_PTRACE);
			int bnetfd;
			if((bnetfd = openat(bfd, "ns/net", 0)) == -1) {
				perror("openat");
				return 1;
			}
			DROP_CAPABILITY(CAP_SYS_PTRACE);

			RAISE_CAPABILITY(CAP_SYS_ADMIN);
			if(setns(bnetfd, CLONE_NEWNET) == -1) {
				perror("setns");
				return 1;
			}
			DROP_CAPABILITY(CAP_SYS_ADMIN);

			RESOLVE_SELF_NS("net", &dev, &inode);

			if(dev == orig_net_dev && inode == orig_net_inode) {
				fprintf(stderr, "Process %u was not started with -n option, or has not unshared network namespace yet.\n", bpid);
				return 1;
			}

			char ppidstr[64];
			if(snprintf(ppidstr, sizeof(ppidstr), "%jd", (intmax_t)getppid()) >= sizeof(ppidstr)) {
				perror("snprintf");
				exit(2);
			}

			// TODO(ivanq): Replace with RTNETLINK
			if(type == TYPE_VETH) {
				execl("/sbin/ip", "/sbin/ip", "link", "add", "name", bname, "type", "veth", "peer", "name", aname, "netns", ppidstr, NULL);
			} else {
				if(execl_ret("/sbin/ip", "/sbin/ip", "link", "add", tmpname, "link", bname, "type", argv[2], "mode", argv[5], NULL) == 0) {
					if(execl_ret("/sbin/ip", "/sbin/ip", "link", "set", "dev", tmpname, "netns", ppidstr, NULL) == 0) {
						return 0;
					}
					execl_ret("/sbin/ip", "/sbin/ip", "link", "del", tmpname, NULL);
				}
				return 1;
			}
			perror("execl");
			return 0;
		}

		// Wait for child
		int retcode;
		if(waitpid(child, &retcode, 0) == -1) {
			perror("waitpid");
			return 2;
		}
		if(!WIFEXITED(retcode)) {
			fprintf(stderr, "Helper process crashed.\n");
			return 2;
		}
		int ret = WEXITSTATUS(retcode);
		if(ret != 0) {
			return ret;
		}

		if(type == TYPE_VETH) {
			return 0;
		}

		if(execl_ret("/sbin/ip", "/sbin/ip", "link", "set", "dev", tmpname, "name", aname, NULL) == 0) {
			return 0;
		}
		execl_ret("/sbin/ip", "/sbin/ip", "link", "del", tmpname, NULL);
		return 1;
	} else if(argc >= 2 && strlen(argv[1]) >= 2 && strncmp(argv[1], "-j", 2) == 0) {
		int fds[16];
		size_t cnt_fds = 0;

		int opt;
		while((opt = getopt(argc, argv, "+j:")) != -1) {
			switch(opt) {
				case 'j': {
					errno = 0;
					unsigned long long int fd = strtoull(optarg, NULL, 0);
					if(errno) {
						perror("strtol");
						return 1;
					}
					if(fd > INT_MAX) {
						fprintf(stderr, "File descriptor number is out of bounds.\n");
						return 1;
					}

					if(cnt_fds >= sizeof(fds) / sizeof(fds[0])) {
						fprintf(stderr, "Too many file descriptors.\n");
						return 1;
					}

					fds[cnt_fds++] = fd;
					break;
				}

				default:
					fprintf(stderr, "Incorrect option format.\n");
					return 1;
			}
		}

		if(optind == argc) {
			fprintf(stderr, "No program passed. Use 'true' command if you actually want to continue processes.\n");
			return 1;
		}

		size_t max_buf_sz = 0;
		for(int i = 0; i < cnt_fds; i++) {
			int fd = fds[i];
			size_t buf_sz = fcntl(fd, F_GETPIPE_SZ);
			max_buf_sz = buf_sz > max_buf_sz ? buf_sz : max_buf_sz;
		}

		char* buf = malloc(max_buf_sz);
		if(buf == NULL) {
			fprintf(stderr, "No memory for buffer.\n");
			return 2;
		}

		for(int i = 0; i < cnt_fds; i++) {
			int fd = fds[i];
			size_t buf_sz = fcntl(fd, F_GETPIPE_SZ);
			if(buf_sz > max_buf_sz) {
				fprintf(stderr, "Unexpected pipe buffer size.\n");
				return 2;
			}
			read_autocont(fd, buf, buf_sz);
		}

		int ret = execvp_ret(argv[optind], argv + optind);

		for(int i = 0; i < cnt_fds; i++) {
			int fd = fds[i];
			size_t buf_sz = fcntl(fd, F_GETPIPE_SZ);
			if(buf_sz > max_buf_sz) {
				fprintf(stderr, "Unexpected pipe buffer size.\n");
				return 2;
			}
			read_autocont(fd, buf, buf_sz);
		}

		free(buf);
		return ret;
	}


	char* overlay_options = NULL;
	char* root_path = NULL;
	char* hostname = NULL;
	char* preexec = NULL;
	char* postexec = NULL;

	struct {
		int sys, proc, devpts, devshm, devmqueue, run, tmp;
	} mounts = {0, 0, 0, 0, 0, 0, 0};
	int has_mounts = 0;

	uid_t login_uid = 0;
	gid_t login_gid = 0;
	int change_login_id = 0;

	int new_pid_namespace = 0;

	int ctl_pipe_fd = -1;

	cap_value_t set_amb_caps[128];
	size_t cnt_set_amb_caps = 0;

	int has_slave_root = 0;

	int opt;
	while((opt = getopt(argc, argv, "+inmpUhu:g:DO:R:M:H:L:E:A:P:C:lj")) != -1) {
		switch(opt) {
			case 'i':
				unshare_flags |= CLONE_NEWIPC;
				break;
			case 'n':
				unshare_flags |= CLONE_NEWNET;
				break;
			case 'm':
				unshare_flags |= CLONE_NEWNS;
				break;
			case 'p':
				new_pid_namespace = 1;
				break;
			case 'U':
				unshare_flags |= CLONE_NEWUSER;
				break;
			case 'h':
				unshare_flags |= CLONE_NEWUTS;
				break;

			case 'u':
			case 'g': {
				id_t slave = 0;
				id_t master = opt == 'u' ? getuid() : getgid();
				if(sscanf(optarg, "%u=%u", &slave, &master) < 1) {
					fprintf(stderr, "-%c option uses 'slave=master' format.\n", (char)opt);
					return 1;
				}

				if(slave == 0) {
					has_slave_root = 1;
				}

				IDMapping* map;
				if(opt == 'u') {
					if(cnt_uid_mappings >= sizeof(uid_mappings) / sizeof(uid_mappings[0])) {
						fprintf(stderr, "Too many -u options.\n");
						return 1;
					}
					map = &uid_mappings[cnt_uid_mappings++];
				} else {
					if(cnt_gid_mappings >= sizeof(gid_mappings) / sizeof(gid_mappings[0])) {
						fprintf(stderr, "Too many -g options.\n");
						return 1;
					}
					map = &gid_mappings[cnt_gid_mappings++];
				}

				map->slave = slave;
				map->master = master;
				map->established = getuid() == 0 || master == (opt == 'u' ? getuid() : getgid());
				break;
			}

			case 'D':
				if(getuid() != 0) {
					fprintf(stderr, "-D requires root.\n");
					return 1;
				}
				allow_setgroups = 1;
				break;

			case 'O':
				overlay_options = optarg;
				break;
			case 'R':
				root_path = optarg;
				break;

			case 'M':
				has_mounts = 1;
				for(char* p = optarg; *p; p++) {
					switch(*p) {
						case 's':
							mounts.sys = 1;
							break;
						case 'p':
							mounts.proc = 1;
							break;
						case 'P':
							mounts.devpts = 1;
							break;
						case 'S':
							mounts.devshm = 1;
							break;
						case 'Q':
							mounts.devmqueue = 1;
							break;
						case 'r':
							mounts.run = 1;
							break;
						case 't':
							mounts.tmp = 1;
							break;
						default:
							fprintf(stderr, "Unknown option -M%c.\n", *p);
							return 1;
					}
				}
				break;

			case 'H':
				hostname = optarg;
				break;

			case 'L':
				if(sscanf(optarg, "%u:%u", &login_uid, &login_gid) < 2) {
					fprintf(stderr, "-L option uses 'uid:gid' format.\n");
					return 1;
				}
				change_login_id = 1;
				break;

			case 'E':
				preexec = optarg;
				break;
			case 'A':
				postexec = optarg;
				break;

			case 'P': {
				errno = 0;
				unsigned long long int fd = strtoull(optarg, NULL, 0);
				if(errno) {
					perror("strtol");
					return 1;
				}
				if(fd > INT_MAX) {
					fprintf(stderr, "File descriptor number is out of bounds.\n");
					return 1;
				}
				ctl_pipe_fd = fd;
				break;
			}

			case 'C': {
				cap_value_t cap;
				errno = 0;
				if(cap_from_name(optarg, &cap) == -1) {
					fprintf(stderr, "Unknown capability %s.\n", optarg);
					return 1;
				}

				if(cnt_set_amb_caps >= sizeof(set_amb_caps) / sizeof(set_amb_caps[0])) {
					fprintf(stderr, "Too many -C options.\n");
					return 1;
				}
				set_amb_caps[cnt_set_amb_caps++] = cap;
				break;
			}

			case 'l':
			case 'j':
				fprintf(stderr, "-%c must be the first option\n", (char)opt);
				return 1;

			default:
				fprintf(stderr, "Incorrect option format.\n");
				return 1;
		}
	}

	if(optind == argc) {
		fprintf(stderr, "No program passed.\n");
		return 1;
	} else if(allow_setgroups && !(unshare_flags & CLONE_NEWUSER)) {
		fprintf(stderr, "-D is passed but -U is missing.\n");
		return 1;
	} else if(overlay_options != NULL && !(unshare_flags && CLONE_NEWNS)) {
		fprintf(stderr, "-O is passed but -m is missing.\n");
		return 1;
	} else if(root_path != NULL && !(unshare_flags && CLONE_NEWNS)) {
		fprintf(stderr, "-R is passed but -m is missing.\n");
		return 1;
	} else if(overlay_options != NULL && root_path != NULL) {
		fprintf(stderr, "-O and -R can't be used together.\n");
		return 1;
	} else if(has_mounts && !(unshare_flags & CLONE_NEWNS)) {
		fprintf(stderr, "-M is passed but -m is missing.\n");
		return 1;
	} else if(hostname != NULL && !(unshare_flags & CLONE_NEWUTS)) {
		fprintf(stderr, "-H is passed but -h is missing.\n");
		return 1;
	} else if(change_login_id && !(unshare_flags & CLONE_NEWUSER)) {
		fprintf(stderr, "-L is passed but -U is missing.\n");
		return 1;
	} else if(cnt_set_amb_caps > 0 && !(unshare_flags & CLONE_NEWUSER)) {
		fprintf(stderr, "-C is passed but -U is missing.\n");
		return 1;
	} else if((unshare_flags & CLONE_NEWUSER) && !has_slave_root) {
		fprintf(stderr, "-U is passed but -u 0=... or -u 0 is missing.\n");
		return 1;
	}

	if(cnt_uid_mappings > 0 || cnt_gid_mappings > 0) {
		if(!(unshare_flags & CLONE_NEWUSER)) {
			fprintf(stderr, "-u/-g are passed but -U is missing.\n");
			return 1;
		}

		qsort(uid_mappings, cnt_uid_mappings, sizeof(uid_mappings[0]), id_mapping_cmp);
		qsort(gid_mappings, cnt_gid_mappings, sizeof(gid_mappings[0]), id_mapping_cmp);

		for(;;) {
			errno = 0;
			struct passwd* ent = getpwent();
			if(ent == NULL) {
				if(errno == 0) {
					break;
				} else {
					perror("getpwent");
					return 2;
				}
			}

			// UID mapping
			IDMapping* map;
			if((map = binsearch_mapping(ent->pw_uid, cnt_uid_mappings, uid_mappings)) != NULL) {
				if(map->established) {
					continue;
				}
				assert(can_masquerade_pwent(ent, 1));
				map->established = 1;
			}

			// GID mapping
			if((map = binsearch_mapping(ent->pw_gid, cnt_gid_mappings, gid_mappings)) != NULL) {
				if(map->established) {
					continue;
				}
				if(can_masquerade_pwent(ent, 0)) {
					map->established = 1;
				}
			}
		}
		endpwent();

		for(size_t i = 0; i < cnt_uid_mappings; i++) {
			if(!uid_mappings[i].established) {
				fprintf(stderr, "UID mapping %u=%u was not established (e.g. master UID does not exist).\n", uid_mappings[i].slave, uid_mappings[i].master);
				return 1;
			}
		}

		for(size_t i = 0; i < cnt_gid_mappings; i++) {
			if(!gid_mappings[i].established) {
				fprintf(stderr, "GID mapping %u=%u was not established (e.g. .masquerade was not found).\n", gid_mappings[i].slave, gid_mappings[i].master);
				return 1;
			}
		}
	}


	// unshare_flags |= CLONE_INTO_CGROUP;
	// unshare_flags |= CLONE_NEWCGROUP;

	int pipefd[2];
	if(pipe(pipefd) == -1) {
		perror("pipe");
		return 2;
	}

	pid_t child;
	char* stack = malloc(STACK_SIZE);
	if((child = clone(helper_proc, stack + STACK_SIZE, SIGCHLD, pipefd)) == -1) {
		perror("clone");
		return 2;
	}

	// Don't read from pipe
	if(close(pipefd[0]) == -1) {
		perror("close");
		return 2;
	}


	try_unshare(unshare_flags);

	// Close writing side after unsharing
	if(close(pipefd[1]) == -1) {
		perror("close");
		return 2;
	}

	// Wait for child
	int retcode;
	if(waitpid(child, &retcode, 0) == -1) {
		perror("waitpid");
		return 2;
	}
	if(!WIFEXITED(retcode)) {
		fprintf(stderr, "Helper process crashed.\n");
		return 2;
	}
	if(WEXITSTATUS(retcode) != 0) {
		return WEXITSTATUS(retcode);
	}

	free(stack);


	if(unshare_flags & CLONE_NEWUSER) {
		// Become root inside namespace
		if(seteuid(0) == -1) {
			perror("seteuid");
			return 1;
		}

		cap_proc = cap_get_proc();
		if(cap_proc == NULL) {
			perror("cap_get_proc");
			return 2;
		}
	}


	if(hostname != NULL) {
		if(sethostname(hostname, strlen(hostname)) == -1) {
			perror("sethostname");
			return 2;
		}
	}


	if(preexec) {
		if(system(preexec) != 0) {
			fprintf(stderr, "Preexec script has non-zero exit code.\n");
			return 1;
		}
	}


	if(new_pid_namespace) {
		try_unshare(CLONE_NEWPID);

		pid_t child = fork();
		if(child == -1) {
			perror("fork");
			return 2;
		} else if(child != 0) {
			copy_child_exitcode(child);
		}

		if(prctl(PR_SET_PDEATHSIG, SIGKILL) == -1) {
			perror("prctl");
		}

		// Fork twice to prevent child from overriding PR_SET_PDEATHSIG
		child = fork();
		if(child == -1) {
			perror("fork");
			return 2;
		} else if(child != 0) {
			copy_child_exitcode(child);
		}
	}


	if(unshare_flags & CLONE_NEWNS) {
		// mount --make-rprivate
		if(mount(NULL, "/", NULL, MS_PRIVATE | MS_REC, NULL) == -1) {
			perror("mount");
			return 2;
		}

		int change_root = overlay_options != NULL || root_path != NULL;
		if(change_root) {
			// Re-mount /tmp to get a new private temporary storage
			// mount -t tmpfs none /tmp
			if(mount("none", "/tmp", "tmpfs", 0, NULL) == -1) {
				perror("mount");
				return 2;
			}

			// Create mounting point inside /tmp
			if(mkdir("/tmp/mnt", 0700) == -1) {
				perror("mkdir");
				return 2;
			}

			if(overlay_options != NULL) {
				// Mount overlayfs
				// mount -t overlay none -o "..." /tmp/mnt
				if(mount("none", "/tmp/mnt", "overlay", 0, overlay_options) == -1) {
					perror("mount");
					return 1;
				}
			} else if(root_path != NULL) {
				// Bind-mount
				// mount --rbind "..." /tmp/mnt
				if(mount(root_path, "/tmp/mnt", NULL, MS_BIND | MS_REC, NULL) == -1) {
					perror("mount");
					return 1;
				}
			} else {
				assert(0);
			}

			// Mount virtual filesystems
			if(mounts.sys) {
				// mount -t sysfs none /tmp/mnt/sys
				if(mount("none", "/tmp/mnt/sys", "sysfs", 0, NULL) == -1) {
					perror("mount(/sys)");
					return 1;
				}
			}
			if(mounts.proc) {
				if(new_pid_namespace) {
					// mount -t proc proc /tmp/mnt/proc
					if(mount("proc", "/tmp/mnt/proc", "proc", 0, NULL) == -1) {
						perror("mount(/proc)");
						return 1;
					}
				} else {
					// mount --rbind /proc /tmp/mnt/proc
					if(mount("/proc", "/tmp/mnt/proc", NULL, MS_BIND | MS_REC, NULL) == -1) {
						perror("mount");
						return 1;
					}
				}
			}
			if(mounts.devpts) {
				// mount -t devpts devpts /tmp/mnt/dev/pts
				if(mount("devpts", "/tmp/mnt/dev/pts", "devpts", 0, NULL) == -1) {
					perror("mount(/dev/pts)");
					return 1;
				}
			}
			if(mounts.devshm) {
				// mount -t tmpfs tmpfs /tmp/mnt/dev/shm
				if(mount("tmpfs", "/tmp/mnt/dev/shm", "tmpfs", 0, NULL) == -1) {
					perror("mount(/dev/shm)");
					return 1;
				}
			}
			if(mounts.devmqueue) {
				// mount -t mqueue mqueue /tmp/mnt/dev/mqueue
				if(mount("mqueue", "/tmp/mnt/dev/mqueue", "mqueue", 0, NULL) == -1) {
					perror("mount(/dev/mqueue)");
					return 1;
				}
			}
			if(mounts.run) {
				// mount -t tmpfs tmpfs /tmp/mnt/run
				if(mount("tmpfs", "/tmp/mnt/run", "tmpfs", 0, NULL) == -1) {
					perror("mount(/run)");
					return 1;
				}
			}
			if(mounts.tmp) {
				// mount -t tmpfs tmpfs /tmp/mnt/tmp
				if(mount("tmpfs", "/tmp/mnt/tmp", "tmpfs", 0, NULL) == -1) {
					perror("mount(/tmp)");
					return 1;
				}
			}
		}

		if(change_root) {
			// Pivot root
			if(chdir("/tmp/mnt") == -1) {
				perror("chdir");
				return 2;
			}
			if(syscall(SYS_pivot_root, ".", ".") == -1) {
				perror("pivot_root");
				return 2;
			}
			if(umount2(".", MNT_DETACH) == -1) {
				perror("umount2");
				return 2;
			}
		}
	}


	if(postexec) {
		if(system(postexec) != 0) {
			fprintf(stderr, "Postexec script has non-zero exit code.\n");
			return 1;
		}
	}


	if(prctl(PR_SET_KEEPCAPS, 1, 0, 0, 0) == -1) {
		perror("prctl");
		return 2;
	}


	if(change_login_id) {
		// Change UID/GID
		if(setgid(login_gid) == -1) {
			perror("setgid");
			return 1;
		}
		if(setuid(login_uid) == -1) {
			perror("setuid");
			return 1;
		}

		cap_proc = cap_get_proc();
		if(cap_proc == NULL) {
			perror("cap_get_proc");
			return 2;
		}
	}


	if(cnt_set_amb_caps > 0) {
		if(cap_set_flag(cap_proc, CAP_EFFECTIVE, cnt_set_amb_caps, set_amb_caps, CAP_SET) == -1) {
			perror("cap_set_flag");
			return 2;
		}
		if(cap_set_flag(cap_proc, CAP_INHERITABLE, cnt_set_amb_caps, set_amb_caps, CAP_SET) == -1) {
			perror("cap_set_flag");
			return 2;
		}
		if(cap_set_proc(cap_proc) == -1) {
			perror("cap_set_proc");
			return 2;
		}
		RAISE_CAPABILITY(CAP_SETPCAP);
		for(size_t i = 0; i < cnt_set_amb_caps; i++) {
			if(cap_set_ambient(set_amb_caps[i], CAP_SET) == -1) {
				perror("cap_set_ambient");
				return 1;
			}
		}
		DROP_CAPABILITY(CAP_SETPCAP);
	}


	if(ctl_pipe_fd != -1) {
		// FIXME(ivanq): Check if this synchronization method is reliable
		// https://unix.stackexchange.com/questions/604473/how-do-i-feed-data-to-a-pipe-until-its-full-no-more-no-less
		size_t buf_sz = fcntl(ctl_pipe_fd, F_GETPIPE_SZ);

		char* buf = malloc(buf_sz);
		memset(buf, 'A', buf_sz);

		write_autocont(ctl_pipe_fd, buf, buf_sz);
		write_autocont(ctl_pipe_fd, buf, buf_sz);
		write_autocont(ctl_pipe_fd, "A", 1);

		free(buf);

		if(close(ctl_pipe_fd) == -1) {
			perror("close");
			return 2;
		}
	}


	if(execvp(argv[optind], argv + optind) == -1) {
		perror("execvp");
		return 1;
	}

	return 0;
}
